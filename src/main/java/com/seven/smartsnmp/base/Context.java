package com.seven.smartsnmp.base;

import com.seven.smartsnmp.config.DiscoverConfig;
import com.seven.smartsnmp.discover.DeviceTypeInfo;
import org.snmp4j.mp.SnmpConstants;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;

/**
 * @author Admin
 */
public class Context {

    static {
        DeviceTypeInfo type = new DeviceTypeInfo();
        type.setDeviceProductType("Windows 2003");
        type.setLogicType("计算机");
        List<String> descs = new Vector();
        descs.add("1.3.6.1.4.1.311.1.1.3.1.3.1.2");
        type.setTypeOIDCharacter(descs);
        DiscoverConfig.addDeviceType(type);
        type = new DeviceTypeInfo();
        type.setDeviceProductType("HP服务器");
        type.setLogicType("计算机");
        descs = new Vector();
        descs.add("1.3.6.1.4.1.11.2.3.2.3");
        type.setTypeOIDCharacter(descs);
        DiscoverConfig.addDeviceType(type);
        type = new DeviceTypeInfo();
        type.setDeviceProductType("Cisco C2600");
        type.setLogicType("路由交换机");
        descs = new Vector();
        descs.add("192.168.174.12");
        type.setTypeOIDCharacter(descs);
        DiscoverConfig.addDeviceType(type);
        type = new DeviceTypeInfo();
        type.setDeviceProductType("Windows计算机");
        type.setLogicType("计算机");
        descs = new Vector();
        descs.add("windows");
        type.setTypeDescCharacter(descs);
        DiscoverConfig.addDeviceType(type);
        type = new DeviceTypeInfo();
        type.setDeviceProductType("IBM AIX服务器");
        type.setLogicType("计算机");
        descs = new Vector();
        descs.add("AIX");
        type.setTypeDescCharacter(descs);
        DiscoverConfig.addDeviceType(type);
        type = new DeviceTypeInfo();
        type.setDeviceProductType("H3C路由器");
        type.setLogicType("路由器");
        descs = new Vector();
        descs.add("H3C Series Router");
        type.setTypeDescCharacter(descs);
        DiscoverConfig.addDeviceType(type);
    }

    /**
     * 版本号码
     */
    public static final List<Integer> vlist = Arrays.asList(SnmpConstants.version2c);
    /**
     * snmp密码列表
     */
    public static final List<String> clist = Arrays.asList("hnist","public");//, "swcfsfyxylxydt_2017", "sam2007", "swcfsfyxylxydt", "Hnustsnmpv2");


}
