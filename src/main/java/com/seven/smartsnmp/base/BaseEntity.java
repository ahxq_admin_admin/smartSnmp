package com.seven.smartsnmp.base;

import java.io.Serializable;

/**
 * Created by heer on 2018/7/30.
 */
public abstract class BaseEntity implements Serializable {

    public String nodeName;
    public String tableIndexOID;

    public BaseEntity() {
    }

    public abstract String getMappingOID();

    public String getNodeName() {
        return this.nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public void setTableIndexOID(String tableIndexOID) {
        this.tableIndexOID = tableIndexOID;
    }

    public String getTableIndexOID() {
        return this.tableIndexOID;
    }
}
