package com.seven.smartsnmp.base;

public class SNMPSysConfig {

    public static final String mibsPath = "mibs";
    public static int SnmpTrapPort = 162;
    public static String SnmpTrapCommunity = "public";
    public static int SnmpTimeout = 500;
    public static int SnmpRetry = 0;
    public static boolean isProxyGate = false;
    public static int SNMPThreadPoolSize = 200;//默认线程是200

    public SNMPSysConfig() {
    }

    public static String getObjectSNMPVersion() {
        return "2011.11.01";
    }




}
