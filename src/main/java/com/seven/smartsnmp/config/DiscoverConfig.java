package com.seven.smartsnmp.config;

import com.seven.smartsnmp.discover.DeviceTypeInfo;
import com.seven.smartsnmp.discover.ResourceSearchMothType;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class DiscoverConfig {
    public static boolean isDebug = false;
    public static boolean isSearchLogicInfo = true;
    public static boolean enableBasicInfoMainMac = true;
    public static boolean enableBasicInfoIPAddress = true;
    public static boolean enableBasicInfoIfTable = true;
    public static boolean enableBasicInfoMacIP = true;
    public static boolean enableDiscoverLLDP = true;
    public static boolean enableDiscoverCDP = true;
    public static boolean enableDiscoverStp = false;
    public static boolean enableDiscoverOspfNbr = true;
    public static boolean enableDiscoverPortForward = true;
    public static boolean isUserArpDiscover = false;
    public static boolean isIPRoamingAll = true;
    public static boolean isUseSubHookDiscover = true;
    public static boolean isUserFuzzyLink = true;

    private static List<DeviceTypeInfo> nodeTypeList = new CopyOnWriteArrayList<>();

    private static List<ResourceSearchMothType> resourceTypeList = new CopyOnWriteArrayList();
    public DiscoverConfig() {
    }

    public static synchronized void addDeviceType(DeviceTypeInfo deviceTypeInfo) {
        nodeTypeList.add(deviceTypeInfo);
    }

    public static synchronized void clearAllDeviceType() {
        nodeTypeList.clear();
    }

    public static List<DeviceTypeInfo> getAllDeviceType() {
        return nodeTypeList;
    }
    public static synchronized void addResourceType(ResourceSearchMothType var0) {
        resourceTypeList.add(var0);
    }

    public static synchronized void clearAllResourceSearchMoth() {
        resourceTypeList.clear();
    }

    public static List<ResourceSearchMothType> getAllResourceType() {
        return resourceTypeList;
    }

}
