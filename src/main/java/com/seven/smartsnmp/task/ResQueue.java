package com.seven.smartsnmp.task;

import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author seven
 * @create 2021-01-05 10:42
 **/
public class ResQueue {
    private static final LinkedBlockingQueue<HashMap> linkedBlockingQueue = new LinkedBlockingQueue<>();

    public static LinkedBlockingQueue<HashMap> getLinkedBlockingQueue() {
        return linkedBlockingQueue;
    }
}
