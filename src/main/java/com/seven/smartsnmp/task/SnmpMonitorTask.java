package com.seven.smartsnmp.task;

import com.seven.smartsnmp.base.MyException;
import com.seven.smartsnmp.snmp.SNMPFactory;
import com.seven.smartsnmp.snmp.SNMPTarget;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * @author seven
 * @create 2021-01-05 9:44
 **/
public class SnmpMonitorTask implements Runnable {

    private MonitorType monitorType;
    private SNMPTarget snmpTarget;
    private String taskName;


    public SnmpMonitorTask(MonitorType monitorType, SNMPTarget snmpTarget, String taskName) {
        this.monitorType = monitorType;
        this.snmpTarget = snmpTarget;
        this.taskName = taskName;
    }


    @Override
    public void run() {
        try {
            if (monitorType.getType().equals("table")) {
                List list = SNMPFactory.getSNMPAPI().getAllTableData(monitorType.getT().getClass(), snmpTarget);
                HashMap<String, Object> map = new HashMap<>();
                map.put(taskName, list);
                ResQueue.getLinkedBlockingQueue().add(map);
            }
            if (monitorType.getType().equals("group")) {
                Object obj = monitorType.getT();
                SNMPFactory.getSNMPAPI().getMibObject(obj, snmpTarget);
                HashMap<String, Object> map = new HashMap<>();
                map.put(taskName, obj);
                ResQueue.getLinkedBlockingQueue().add(map);
            }
            if (monitorType.getType().equals("String")) {
                String res = SNMPFactory.getSNMPAPI().getOIDValue(monitorType.getT().toString(), snmpTarget);
                HashMap<String, Object> map = new HashMap<>();
                map.put(taskName, res);
                ResQueue.getLinkedBlockingQueue().add(map);
            }
            if (monitorType.getType().equals("long")) {
                Long res = SNMPFactory.getSNMPAPI().getOIDLongValue(monitorType.getT().toString(), snmpTarget);
                HashMap<String, Object> map = new HashMap<>();
                map.put(taskName, res);
                ResQueue.getLinkedBlockingQueue().add(map);
            }
        } catch (MyException | IOException e) {
            e.printStackTrace();
        }
    }
}
