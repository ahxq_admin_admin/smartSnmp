package com.seven.smartsnmp.proxy.util;


import com.seven.smartsnmp.base.BaseInterface;
import com.seven.smartsnmp.proxy.DynamicProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class ProxyUtil {

    private ThreadLocal<BaseInterface> subject = new ThreadLocal<>();

    public ProxyUtil(BaseInterface baseInterface) {
        InvocationHandler handler = new DynamicProxy(baseInterface);
        BaseInterface baseInterface1 = (BaseInterface) Proxy.newProxyInstance(
                handler.getClass().getClassLoader()
                , baseInterface.getClass().getInterfaces()
                , handler);
        subject.set(baseInterface1);
    }

    public String getOIDMapping() {
        return subject.get().getMappingOID();
    }

    public Map getIndexMap() {
        return subject.get().getIndexMap();
    }

    public Map getValues() {
        return subject.get().getValues();
    }

    public String getTableIndexOID() {
        return subject.get().getTableIndexOID();
    }

    public String getIndexValue() {
        return subject.get().getIndexValue();
    }

    public BaseInterface setValue(List list) {
        return subject.get().setValue(list);
    }
}
