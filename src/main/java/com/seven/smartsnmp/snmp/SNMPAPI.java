package com.seven.smartsnmp.snmp;


import com.seven.smartsnmp.base.MyException;
import net.percederberg.mibble.MibValueSymbol;

import java.io.IOException;
import java.util.List;

public interface SNMPAPI {

    int RowStatusEntryActive = 1;
    int RowStatusEntryAdd = 4;
    int RowStatusEntryDel = 6;
    String MessageNoSuchOID = "MIB OID不存在";
    String MessageSNMPTimeOut = "SNMP没有应答";

    /**
     * 根据oid解析mib文件
     *
     * @param oid
     * @return
     * @throws MyException
     */
    MibValueSymbol getMibSymbolByOid(String oid) throws MyException;

    /**
     * 检查设备snmp是支持
     *
     * @param snmpTarget
     * @throws MyException
     */
    boolean checkSnmpAgentActive(SNMPTarget snmpTarget) throws MyException;

    /**
     * 设置mib节点的数值
     *
     * @param OID
     * @param value
     * @param var3
     * @param snmpTarget
     * @throws MyException
     */
    void setOIDValue(String OID, String value, String var3, SNMPTarget snmpTarget) throws MyException;

    /**
     * h获取oid的值
     *
     * @param oid
     * @param snmpTarget
     * @return
     * @throws MyException
     */
    String getOIDValue(String oid, SNMPTarget snmpTarget) throws MyException;

    /**
     * 获取Long型的oid值
     *
     * @param oid
     * @param snmpTarget
     * @return
     * @throws MyException
     */
    Long getOIDLongValue(String oid, SNMPTarget snmpTarget) throws MyException;

    /**
     * 获取下一个节点的值
     *
     * @param oid
     * @param snmpTarget
     * @return
     * @throws MyException
     */
    String getNextOIDValue(String oid, SNMPTarget snmpTarget) throws MyException;

    /**
     * 获取snmp组的值
     *
     * @param obj
     * @param snmpTarget
     * @return
     * @throws MyException
     * @throws IOException
     */
    Object getMibObject(Object obj, SNMPTarget snmpTarget) throws MyException, IOException;

    /**
     * 更新snmp数据
     *
     * @param obj
     * @param snmpTarget
     * @throws MyException
     */
    void update(Object obj, SNMPTarget snmpTarget) throws MyException;

    /**
     * 获取全部数
     *
     * @param clazz
     * @param snmpTarget
     * @return
     * @throws MyException
     */
    List getAllTableData(Class clazz, SNMPTarget snmpTarget) throws MyException;

    /**
     * 获取全部表格数据的值
     *
     * @param snmpTarget
     * @param oids
     * @return
     * @throws MyException
     * @throws IOException
     */
    List<List<String>> getAllOIDTableData(SNMPTarget snmpTarget, List<String> oids) throws MyException, IOException;

    /**
     * 更新snmp表格数据
     *
     * @param object
     * @param snmpTarget
     * @throws MyException
     * @throws IOException
     */
    void addTableRow(Object object, SNMPTarget snmpTarget) throws MyException, IOException;

    /**
     * 删除snmp表格数据
     *
     * @param object
     * @param snmpTarget
     * @throws MyException
     */
    void delTableRow(Object object, SNMPTarget snmpTarget) throws MyException;
}
