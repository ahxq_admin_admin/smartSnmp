package com.seven.smartsnmp.snmp;

import java.io.Serializable;

public class SNMPTarget implements Serializable {

    public String nodeIP;
    public long targetSnmpTimeout=5000L;
    public int targetSnmpRetry;
    public int snmpVersion;
    public String readCommunity;
    public String writeCommunity;
    public String proxySNMPGateIP;
    public int port;
    public String v3User;
    public int v3SecurityLevel;
    public String v3EngineName;
    public String v3ContextName;
    public int v3AuthProtocol;
    public String v3AuthPasswd;
    public int v3PrivacyProtocol;
    public String v3PrivacyPasswd;
    public static final int VERSION1 = 0;
    public static final int VERSION2C = 1;
    public static final int VERSION3 = 3;
    public static final int V3SecurityLevelNOAUTHNOPRIV = 1;
    public static final int V3SecurityLevelAUTHNOPRIV = 2;
    public static final int V3SecurityLevelAUTHPRIV = 3;
    public static final int V3AuthProtocolMD5 = 1;
    public static final int V3AuthProtocolSHA = 2;
    public static final int V3PrivacyProtocolDES = 1;

    public SNMPTarget()
    {

    }

    public SNMPTarget copyTarget()
    {
        SNMPTarget localSNMPTarget = new SNMPTarget();
        SNMPTarget tmp10_9 = localSNMPTarget;
        SNMPTarget tmp11_10 = tmp10_9;
        SNMPTarget tmp17_16 = localSNMPTarget;
        SNMPTarget tmp23_22 = localSNMPTarget;
        SNMPTarget tmp29_28 = localSNMPTarget;
        SNMPTarget tmp35_34 = localSNMPTarget;
        localSNMPTarget.nodeIP = this.nodeIP;
        localSNMPTarget.port = this.port;
        tmp35_34.proxySNMPGateIP = this.proxySNMPGateIP;
        tmp35_34.readCommunity = this.readCommunity;
        localSNMPTarget.snmpVersion = this.snmpVersion;
        tmp29_28.targetSnmpRetry = this.targetSnmpRetry;
        tmp29_28.targetSnmpTimeout = this.targetSnmpTimeout;
        localSNMPTarget.v3AuthPasswd = this.v3AuthPasswd;
        tmp23_22.v3AuthProtocol = this.v3AuthProtocol;
        tmp23_22.v3ContextName = this.v3ContextName;
        localSNMPTarget.v3EngineName = this.v3EngineName;
        tmp17_16.v3PrivacyPasswd = this.v3PrivacyPasswd;
        tmp17_16.v3PrivacyProtocol = this.v3PrivacyProtocol;
        localSNMPTarget.v3SecurityLevel = this.v3SecurityLevel;
        tmp11_10.v3User = this.v3User;
        tmp10_9.writeCommunity = this.writeCommunity;
        return tmp11_10;
    }

    public String getNodeIP() {
        return nodeIP;
    }

    public void setNodeIP(String nodeIP) {
        this.nodeIP = nodeIP;
    }

    public long getTargetSnmpTimeout() {
        return targetSnmpTimeout;
    }

    public void setTargetSnmpTimeout(int targetSnmpTimeout) {
        this.targetSnmpTimeout = targetSnmpTimeout;
    }

    public int getTargetSnmpRetry() {
        return targetSnmpRetry;
    }

    public void setTargetSnmpRetry(int targetSnmpRetry) {
        this.targetSnmpRetry = targetSnmpRetry;
    }

    public int getSnmpVersion() {
        return snmpVersion;
    }

    public void setSnmpVersion(int snmpVersion) {
        this.snmpVersion = snmpVersion;
    }

    public String getReadCommunity() {
        return readCommunity;
    }

    public void setReadCommunity(String readCommunity) {
        this.readCommunity = readCommunity;
    }

    public String getWriteCommunity() {
        return writeCommunity;
    }

    public void setWriteCommunity(String writeCommunity) {
        this.writeCommunity = writeCommunity;
    }

    public String getProxySNMPGateIP() {
        return proxySNMPGateIP;
    }

    public void setProxySNMPGateIP(String proxySNMPGateIP) {
        this.proxySNMPGateIP = proxySNMPGateIP;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getV3User() {
        return v3User;
    }

    public void setV3User(String v3User) {
        this.v3User = v3User;
    }

    public int getV3SecurityLevel() {
        return v3SecurityLevel;
    }

    public void setV3SecurityLevel(int v3SecurityLevel) {
        this.v3SecurityLevel = v3SecurityLevel;
    }

    public String getV3EngineName() {
        return v3EngineName;
    }

    public void setV3EngineName(String v3EngineName) {
        this.v3EngineName = v3EngineName;
    }

    public String getV3ContextName() {
        return v3ContextName;
    }

    public void setV3ContextName(String v3ContextName) {
        this.v3ContextName = v3ContextName;
    }

    public int getV3AuthProtocol() {
        return v3AuthProtocol;
    }

    public void setV3AuthProtocol(int v3AuthProtocol) {
        this.v3AuthProtocol = v3AuthProtocol;
    }

    public String getV3AuthPasswd() {
        return v3AuthPasswd;
    }

    public void setV3AuthPasswd(String v3AuthPasswd) {
        this.v3AuthPasswd = v3AuthPasswd;
    }

    public int getV3PrivacyProtocol() {
        return v3PrivacyProtocol;
    }

    public void setV3PrivacyProtocol(int v3PrivacyProtocol) {
        this.v3PrivacyProtocol = v3PrivacyProtocol;
    }

    public String getV3PrivacyPasswd() {
        return v3PrivacyPasswd;
    }

    public void setV3PrivacyPasswd(String v3PrivacyPasswd) {
        this.v3PrivacyPasswd = v3PrivacyPasswd;
    }

    public static int getVERSION1() {
        return VERSION1;
    }

    public static int getVERSION2C() {
        return VERSION2C;
    }

    public static int getVERSION3() {
        return VERSION3;
    }

    public static int getV3SecurityLevelNOAUTHNOPRIV() {
        return V3SecurityLevelNOAUTHNOPRIV;
    }

    public static int getV3SecurityLevelAUTHNOPRIV() {
        return V3SecurityLevelAUTHNOPRIV;
    }

    public static int getV3SecurityLevelAUTHPRIV() {
        return V3SecurityLevelAUTHPRIV;
    }

    public static int getV3AuthProtocolMD5() {
        return V3AuthProtocolMD5;
    }

    public static int getV3AuthProtocolSHA() {
        return V3AuthProtocolSHA;
    }

    public static int getV3PrivacyProtocolDES() {
        return V3PrivacyProtocolDES;
    }
}