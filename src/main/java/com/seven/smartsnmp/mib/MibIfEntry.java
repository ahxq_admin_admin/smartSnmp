package com.seven.smartsnmp.mib;


import com.seven.smartsnmp.base.OMMappingInfo;

/**
 * @author seven
 * @create 2019-06-06 3:34
 **/
public class MibIfEntry extends OMMappingInfo {
    private int ifIndex;
    private String ifDescr;
    private int ifType;
    private int ifMtu;
    private long ifSpeed;
    private String ifPhysAddress;
    private int ifAdminStatus;
    private int ifOperStatus;
    private long ifLastChange;
    private long ifInOctets;
    private long ifInUcastPkts;
    private long ifInNUcastPkts;
    private long ifInDiscards;
    private long ifInErrors;
    private long ifInUnknownProtos;
    private long ifOutOctets;
    private long ifOutUcastPkts;
    private long ifOutNUcastPkts;
    private long ifOutDiscards;
    private long ifOutErrors;
    private long ifOutQLen;
    private String ifSpecific;
    @Override
    public String toString() {
        return "ifIndex=" + this.ifIndex + "|" + "ifDescr=" + this.ifDescr + "|" + "ifType=" + this.ifType + "|" + "ifMtu=" + this.ifMtu + "|" + "ifSpeed=" + this.ifSpeed + "|" + "ifPhysAddress=" + this.ifPhysAddress + "|" + "ifAdminStatus=" + this.ifAdminStatus + "|" + "ifOperStatus=" + this.ifOperStatus + "|" + "ifLastChange=" + this.ifLastChange + "|" + "ifInOctets=" + this.ifInOctets + "|" + "ifInUcastPkts=" + this.ifInUcastPkts + "|" + "ifInNUcastPkts=" + this.ifInNUcastPkts + "|" + "ifInDiscards=" + this.ifInDiscards + "|" + "ifInErrors=" + this.ifInErrors + "|" + "ifInUnknownProtos=" + this.ifInUnknownProtos + "|" + "ifOutOctets=" + this.ifOutOctets + "|" + "ifOutUcastPkts=" + this.ifOutUcastPkts + "|" + "ifOutNUcastPkts=" + this.ifOutNUcastPkts + "|" + "ifOutDiscards=" + this.ifOutDiscards + "|" + "ifOutErrors=" + this.ifOutErrors + "|" + "ifOutQLen=" + this.ifOutQLen + "|" + "ifSpecific=" + this.ifSpecific + "|";
    }
    @Override
    public String getMappingOID() {
        return "1.3.6.1.2.1.2.2.1";
    }


    public int getIfIndex() {
        return ifIndex;
    }

    public void setIfIndex(int ifIndex) {
        this.ifIndex = ifIndex;
    }

    public String getIfDescr() {
        return ifDescr;
    }

    public void setIfDescr(String ifDescr) {
        this.ifDescr = ifDescr;
    }

    public int getIfType() {
        return ifType;
    }

    public void setIfType(int ifType) {
        this.ifType = ifType;
    }

    public int getIfMtu() {
        return ifMtu;
    }

    public void setIfMtu(int ifMtu) {
        this.ifMtu = ifMtu;
    }

    public long getIfSpeed() {
        return ifSpeed;
    }

    public void setIfSpeed(long ifSpeed) {
        this.ifSpeed = ifSpeed;
    }

    public String getIfPhysAddress() {
        return ifPhysAddress;
    }

    public void setIfPhysAddress(String ifPhysAddress) {
        this.ifPhysAddress = ifPhysAddress;
    }

    public int getIfAdminStatus() {
        return ifAdminStatus;
    }

    public void setIfAdminStatus(int ifAdminStatus) {
        this.ifAdminStatus = ifAdminStatus;
    }

    public int getIfOperStatus() {
        return ifOperStatus;
    }

    public void setIfOperStatus(int ifOperStatus) {
        this.ifOperStatus = ifOperStatus;
    }

    public long getIfLastChange() {
        return ifLastChange;
    }

    public void setIfLastChange(long ifLastChange) {
        this.ifLastChange = ifLastChange;
    }

    public long getIfInOctets() {
        return ifInOctets;
    }

    public void setIfInOctets(long ifInOctets) {
        this.ifInOctets = ifInOctets;
    }

    public long getIfInUcastPkts() {
        return ifInUcastPkts;
    }

    public void setIfInUcastPkts(long ifInUcastPkts) {
        this.ifInUcastPkts = ifInUcastPkts;
    }

    public long getIfInNUcastPkts() {
        return ifInNUcastPkts;
    }

    public void setIfInNUcastPkts(long ifInNUcastPkts) {
        this.ifInNUcastPkts = ifInNUcastPkts;
    }

    public long getIfInDiscards() {
        return ifInDiscards;
    }

    public void setIfInDiscards(long ifInDiscards) {
        this.ifInDiscards = ifInDiscards;
    }

    public long getIfInErrors() {
        return ifInErrors;
    }

    public void setIfInErrors(long ifInErrors) {
        this.ifInErrors = ifInErrors;
    }

    public long getIfInUnknownProtos() {
        return ifInUnknownProtos;
    }

    public void setIfInUnknownProtos(long ifInUnknownProtos) {
        this.ifInUnknownProtos = ifInUnknownProtos;
    }

    public long getIfOutOctets() {
        return ifOutOctets;
    }

    public void setIfOutOctets(long ifOutOctets) {
        this.ifOutOctets = ifOutOctets;
    }

    public long getIfOutUcastPkts() {
        return ifOutUcastPkts;
    }

    public void setIfOutUcastPkts(long ifOutUcastPkts) {
        this.ifOutUcastPkts = ifOutUcastPkts;
    }

    public long getIfOutNUcastPkts() {
        return ifOutNUcastPkts;
    }

    public void setIfOutNUcastPkts(long ifOutNUcastPkts) {
        this.ifOutNUcastPkts = ifOutNUcastPkts;
    }

    public long getIfOutDiscards() {
        return ifOutDiscards;
    }

    public void setIfOutDiscards(long ifOutDiscards) {
        this.ifOutDiscards = ifOutDiscards;
    }

    public long getIfOutErrors() {
        return ifOutErrors;
    }

    public void setIfOutErrors(long ifOutErrors) {
        this.ifOutErrors = ifOutErrors;
    }

    public long getIfOutQLen() {
        return ifOutQLen;
    }

    public void setIfOutQLen(long ifOutQLen) {
        this.ifOutQLen = ifOutQLen;
    }

    public String getIfSpecific() {
        return ifSpecific;
    }

    public void setIfSpecific(String ifSpecific) {
        this.ifSpecific = ifSpecific;
    }
}
