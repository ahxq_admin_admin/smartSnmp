package com.seven.smartsnmp.mib;


import com.seven.smartsnmp.base.OMMappingInfo;

/**
 * @author seven
 * @create 2019-06-06 3:35
 **/
public class Dot1dBasePortEntry extends OMMappingInfo {
    private int dot1dBasePort;
    private int dot1dBasePortIfIndex;
    private String dot1dBasePortCircuit;
    private long dot1dBasePortDelayExceededDiscards;
    private long dot1dBasePortMtuExceededDiscards;

    public Dot1dBasePortEntry() {
    }

    @Override
    public String toString() {
        return "dot1dBasePort=" + this.dot1dBasePort + "|" + "dot1dBasePortIfIndex=" + this.dot1dBasePortIfIndex + "|" + "dot1dBasePortCircuit=" + this.dot1dBasePortCircuit + "|" + "dot1dBasePortDelayExceededDiscards=" + this.dot1dBasePortDelayExceededDiscards + "|" + "dot1dBasePortMtuExceededDiscards=" + this.dot1dBasePortMtuExceededDiscards + "|";
    }

    @Override
    public String getMappingOID() {
        return "1.3.6.1.2.1.17.1.4.1";
    }

    public int getDot1dBasePort() {
        return this.dot1dBasePort;
    }

    public void setDot1dBasePort(int var1) {
        this.dot1dBasePort = var1;
    }

    public int getDot1dBasePortIfIndex() {
        return this.dot1dBasePortIfIndex;
    }

    public void setDot1dBasePortIfIndex(int var1) {
        this.dot1dBasePortIfIndex = var1;
    }

    public String getDot1dBasePortCircuit() {
        return this.dot1dBasePortCircuit;
    }

    public void setDot1dBasePortCircuit(String var1) {
        this.dot1dBasePortCircuit = var1;
    }

    public long getDot1dBasePortDelayExceededDiscards() {
        return this.dot1dBasePortDelayExceededDiscards;
    }

    public void setDot1dBasePortDelayExceededDiscards(long var1) {
        this.dot1dBasePortDelayExceededDiscards = var1;
    }

    public long getDot1dBasePortMtuExceededDiscards() {
        return this.dot1dBasePortMtuExceededDiscards;
    }

    public void setDot1dBasePortMtuExceededDiscards(long var1) {
        this.dot1dBasePortMtuExceededDiscards = var1;
    }
}
