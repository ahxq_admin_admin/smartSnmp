package com.seven.smartsnmp.mib;


import com.seven.smartsnmp.base.OMMappingInfo;

/**
 * @author seven
 * @create 2019-06-06 3:34
 **/
public class Dot1dTpFdbEntry extends OMMappingInfo {
    private String dot1dTpFdbAddress;
    private int dot1dTpFdbPort;
    private int dot1dTpFdbStatus;

    public Dot1dTpFdbEntry() {
    }
    @Override
    public String toString() {
        return "dot1dTpFdbAddress=" + this.dot1dTpFdbAddress + "|" + "dot1dTpFdbPort=" + this.dot1dTpFdbPort;
    }
    @Override
    public String getMappingOID() {
        return "1.3.6.1.2.1.17.4.3.1";
    }

    public String getDot1dTpFdbAddress() {
        return this.dot1dTpFdbAddress;
    }

    public void setDot1dTpFdbAddress(String var1) {
        this.dot1dTpFdbAddress = var1;
    }

    public int getDot1dTpFdbPort() {
        return this.dot1dTpFdbPort;
    }

    public void setDot1dTpFdbPort(int var1) {
        this.dot1dTpFdbPort = var1;
    }

    public int getDot1dTpFdbStatus() {
        return this.dot1dTpFdbStatus;
    }

    public void setDot1dTpFdbStatus(int var1) {
        this.dot1dTpFdbStatus = var1;
    }
}
