package com.seven.smartsnmp.mib;

import com.seven.smartsnmp.base.OMMappingInfo;

public class MibDiskAndMemoEntry extends OMMappingInfo {
    private int hrStorageIndex;
    private String hrStorageType;
    private String hrStorageDescr;
    private int hrStorageAllocationUnits;
    private int hrStorageSize;
    private int hrStorageUsed;
    private long hrStorageAllocationFailures;

    public MibDiskAndMemoEntry() {
    }

    @Override
    public String toString() {
        return "hrStorageIndex=" + this.hrStorageIndex + "|" + "hrStorageType=" + this.hrStorageType + "|" + "hrStorageDescr=" + this.hrStorageDescr + "|" + "hrStorageAllocationUnits=" + this.hrStorageAllocationUnits + "|" + "hrStorageSize=" + this.hrStorageSize + "|" + "hrStorageUsed=" + this.hrStorageUsed + "|" + "hrStorageAllocationFailures=" + this.hrStorageAllocationFailures + "|";
    }

    @Override
    public String getMappingOID() {
        return "1.3.6.1.2.1.25.2.3.1";
    }

    public int getHrStorageIndex() {
        return this.hrStorageIndex;
    }

    public void setHrStorageIndex(int var1) {
        this.hrStorageIndex = var1;
    }

    public String getHrStorageType() {
        return this.hrStorageType;
    }

    public void setHrStorageType(String var1) {
        this.hrStorageType = var1;
    }

    public String getHrStorageDescr() {
        return this.hrStorageDescr;
    }

    public void setHrStorageDescr(String var1) {
        this.hrStorageDescr = var1;
    }

    public int getHrStorageAllocationUnits() {
        return this.hrStorageAllocationUnits;
    }

    public void setHrStorageAllocationUnits(int var1) {
        this.hrStorageAllocationUnits = var1;
    }

    public int getHrStorageSize() {
        return this.hrStorageSize;
    }

    public void setHrStorageSize(int var1) {
        this.hrStorageSize = var1;
    }

    public int getHrStorageUsed() {
        return this.hrStorageUsed;
    }

    public void setHrStorageUsed(int var1) {
        this.hrStorageUsed = var1;
    }

    public long getHrStorageAllocationFailures() {
        return this.hrStorageAllocationFailures;
    }

    public void setHrStorageAllocationFailures(long var1) {
        this.hrStorageAllocationFailures = var1;
    }
}


