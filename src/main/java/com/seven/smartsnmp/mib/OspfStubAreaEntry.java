package com.seven.smartsnmp.mib;
//This code is generated by seven!

import com.seven.smartsnmp.base.OMMappingInfo;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class OspfStubAreaEntry extends OMMappingInfo {
	private String tableIndexOID;
	private String ospfStubAreaId;
	private Integer ospfStubTOS;
	private Integer ospfStubMetric;
	private Integer ospfStubStatus;
	private Integer ospfStubMetricType;
	public String getTableIndexOID(){
		return tableIndexOID;
	}
	public void setTableIndexOID(String tableIndexOID){
		this.tableIndexOID=tableIndexOID;
	}
	public String getOspfStubAreaId(){
		return ospfStubAreaId;
	}
	public void setOspfStubAreaId(String ospfStubAreaId){
		this.ospfStubAreaId=ospfStubAreaId;
	}
	public Integer getOspfStubTOS(){
		return ospfStubTOS;
	}
	public void setOspfStubTOS(Integer ospfStubTOS){
		this.ospfStubTOS=ospfStubTOS;
	}
	public Integer getOspfStubMetric(){
		return ospfStubMetric;
	}
	public void setOspfStubMetric(Integer ospfStubMetric){
		this.ospfStubMetric=ospfStubMetric;
	}
	public Integer getOspfStubStatus(){
		return ospfStubStatus;
	}
	public void setOspfStubStatus(Integer ospfStubStatus){
		this.ospfStubStatus=ospfStubStatus;
	}
	public Integer getOspfStubMetricType(){
		return ospfStubMetricType;
	}
	public void setOspfStubMetricType(Integer ospfStubMetricType){
		this.ospfStubMetricType=ospfStubMetricType;
	}
	public String getMappingOID(){
	   return "1.3.6.1.2.1.14.3.1";
}
	public String toString(){
	   return "tableIndexOID="+tableIndexOID+"|"+"ospfStubAreaId="+ospfStubAreaId+"|"+"ospfStubTOS="+ospfStubTOS+"|"+"ospfStubMetric="+ospfStubMetric+"|"+"ospfStubStatus="+ospfStubStatus+"|"+"ospfStubMetricType="+ospfStubMetricType+"|";
	}
	public Integer getIndexCount(){
	   return 2;
     }
	public Map getIndexMap(){
	   Map map = new LinkedHashMap();
	   map.put("ospfStubAreaId",1);
	   map.put("ospfStubTOS",2);
	   map.put("ospfStubMetric",3);
	   map.put("ospfStubStatus",4);
	   map.put("ospfStubMetricType",5);
	   return map;
	}
	public Map getValues(){
	   Map map = new LinkedHashMap();
	   if(getOspfStubAreaId()!=null){
	         map.put("ospfStubAreaId",getOspfStubAreaId());
	     }
	   if(getOspfStubTOS()!=null){
	         map.put("ospfStubTOS",getOspfStubTOS());
	     }
	   if(getOspfStubMetric()!=null){
	         map.put("ospfStubMetric",getOspfStubMetric());
	     }
	   if(getOspfStubStatus()!=null){
	         map.put("ospfStubStatus",getOspfStubStatus());
	     }
	   if(getOspfStubMetricType()!=null){
	         map.put("ospfStubMetricType",getOspfStubMetricType());
	     }
	   return map;
	}
	public String getIndexValue(){
	   if(getIndexCount()==1)
	      return getOspfStubAreaId().toString();
	   return getTableIndexOID();
	 }
	public OspfStubAreaEntry setValue(List list){
	     OspfStubAreaEntry obj = new OspfStubAreaEntry();
	     obj.setOspfStubAreaId((String)list.get(0));
	     obj.setOspfStubTOS((Integer)list.get(1));
	     obj.setOspfStubMetric((Integer)list.get(2));
	     obj.setOspfStubStatus((Integer)list.get(3));
	     obj.setOspfStubMetricType((Integer)list.get(4));
	     return obj;
	 }
}