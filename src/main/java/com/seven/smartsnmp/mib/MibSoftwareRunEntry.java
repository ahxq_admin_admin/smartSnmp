package com.seven.smartsnmp.mib;


import com.seven.smartsnmp.base.OMMappingInfo;

public class MibSoftwareRunEntry extends OMMappingInfo {
    private int hrSWRunIndex;
    private String hrSWRunName;
    private String hrSWRunID;
    private String hrSWRunPath;
    private String hrSWRunParameters;
    private int hrSWRunType;
    private int hrSWRunStatus;

    public MibSoftwareRunEntry() {
    }

    @Override
    public String toString() {
        return "hrSWRunIndex=" + this.hrSWRunIndex + "|" + "hrSWRunName=" + this.hrSWRunName + "|" + "hrSWRunID=" + this.hrSWRunID + "|" + "hrSWRunPath=" + this.hrSWRunPath + "|" + "hrSWRunParameters=" + this.hrSWRunParameters + "|" + "hrSWRunType=" + this.hrSWRunType + "|" + "hrSWRunStatus=" + this.hrSWRunStatus + "|";
    }

    @Override
    public String getMappingOID() {
        return "1.3.6.1.2.1.25.4.2.1";
    }

    public int getHrSWRunIndex() {
        return this.hrSWRunIndex;
    }

    public void setHrSWRunIndex(int var1) {
        this.hrSWRunIndex = var1;
    }

    public String getHrSWRunName() {
        return this.hrSWRunName;
    }

    public void setHrSWRunName(String var1) {
        this.hrSWRunName = var1;
    }

    public String getHrSWRunID() {
        return this.hrSWRunID;
    }

    public void setHrSWRunID(String var1) {
        this.hrSWRunID = var1;
    }

    public String getHrSWRunPath() {
        return this.hrSWRunPath;
    }

    public void setHrSWRunPath(String var1) {
        this.hrSWRunPath = var1;
    }

    public String getHrSWRunParameters() {
        return this.hrSWRunParameters;
    }

    public void setHrSWRunParameters(String var1) {
        this.hrSWRunParameters = var1;
    }

    public int getHrSWRunType() {
        return this.hrSWRunType;
    }

    public void setHrSWRunType(int var1) {
        this.hrSWRunType = var1;
    }

    public int getHrSWRunStatus() {
        return this.hrSWRunStatus;
    }

    public void setHrSWRunStatus(int var1) {
        this.hrSWRunStatus = var1;
    }
}
