package com.seven.smartsnmp.mib;

import com.seven.smartsnmp.base.OMMappingInfo;

public class MibSystem extends OMMappingInfo {
    private String sysDescr;
    private String sysObjectID;
    private long sysUpTime;
    private String sysContact;
    private String sysName;
    private String sysLocation;
    private int sysServices;

    public MibSystem() {
    }

    @Override
    public String toString() {
        return "sysDescr=" + this.sysDescr + "|" + "sysObjectID=" + this.sysObjectID + "|" + "sysUpTime=" + this.sysUpTime + "|" + "sysContact=" + this.sysContact + "|" + "sysName=" + this.sysName + "|" + "sysLocation=" + this.sysLocation + "|" + "sysServices=" + this.sysServices + "|";
    }

    @Override
    public String getMappingOID() {
        return "1.3.6.1.2.1.1";
    }

    public String getSysDescr() {
        return this.sysDescr;
    }

    public void setSysDescr(String var1) {
        this.sysDescr = var1;
    }

    public String getSysObjectID() {
        return this.sysObjectID;
    }

    public void setSysObjectID(String var1) {
        this.sysObjectID = var1;
    }

    public long getSysUpTime() {
        return this.sysUpTime;
    }

    public void setSysUpTime(long var1) {
        this.sysUpTime = var1;
    }

    public String getSysContact() {
        return this.sysContact;
    }

    public void setSysContact(String var1) {
        this.sysContact = var1;
    }

    public String getSysName() {
        return this.sysName;
    }

    public void setSysName(String var1) {
        this.sysName = var1;
    }

    public String getSysLocation() {
        return this.sysLocation;
    }

    public void setSysLocation(String var1) {
        this.sysLocation = var1;
    }

    public int getSysServices() {
        return this.sysServices;
    }

    public void setSysServices(int var1) {
        this.sysServices = var1;
    }
}
