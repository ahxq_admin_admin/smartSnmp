package com.seven.smartsnmp.discover.service;

import com.seven.smartsnmp.base.MyException;
import com.seven.smartsnmp.discover.DeviceInfo;
import com.seven.smartsnmp.discover.DeviceTypeInfo;
import com.seven.smartsnmp.discover.SubNetInfo;
import com.seven.smartsnmp.mib.MibIPRouterEntry;
import com.seven.smartsnmp.snmp.SNMPFactory;
import com.seven.smartsnmp.snmp.SNMPTarget;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class SearchRouterInfoService {
    public SearchRouterInfoService() {
    }

    private ConcurrentLinkedQueue<SubNetInfo> searchSubNet(SNMPTarget snmpTarget) throws MyException {
        ConcurrentLinkedQueue subNetQueue = new ConcurrentLinkedQueue();
        try {
            List list = SNMPFactory.getSNMPAPI().getAllTableData(MibIPRouterEntry.class, snmpTarget);
            Iterator it = list.iterator();
            while(it.hasNext()) {
                MibIPRouterEntry mibIPRouterEntry = (MibIPRouterEntry)it.next();
                String ipRouteDest = mibIPRouterEntry.getIpRouteDest();
                if (ipRouteDest.endsWith(".0") && !ipRouteDest.startsWith("127.") && !ipRouteDest.startsWith("224.") &&
                        !ipRouteDest.equalsIgnoreCase("0.0.0.0") && mibIPRouterEntry.getIpRouteType() == 3) {
                    SubNetInfo subNetInfo = new SubNetInfo();
                    subNetInfo.setNetInfo(ipRouteDest);
                    subNetInfo.setNetRouterIP(mibIPRouterEntry.getIpRouteNextHop());
                    subNetQueue.add(subNetInfo);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return subNetQueue;
    }

    public ConcurrentLinkedQueue<DeviceInfo> searchDeviceByRouterNet(String netInfo, List<SNMPTarget> snmpTargets, boolean isUsePing) {
        ConcurrentLinkedQueue<DeviceInfo> deviceInfos = new ConcurrentLinkedQueue<>();
        ConcurrentLinkedQueue<DeviceInfo> nodeQueue = (new SearchNodeService()).searchDevice(netInfo, snmpTargets, isUsePing);
        Iterator iterator = nodeQueue.iterator();
        while(true) {
            DeviceInfo deviceInfo;
            do {
                if (!iterator.hasNext()) {
                    return deviceInfos;
                }
                deviceInfo = (DeviceInfo)iterator.next();
            } while(!deviceInfo.getDeviceType().equalsLogicType(DeviceTypeInfo.RouterType));
            SNMPTarget snmpTarget = deviceInfo.getSnmpTarget();
            try {
                ConcurrentLinkedQueue subNetQueue = this.searchSubNet(snmpTarget);
                Iterator it = subNetQueue.iterator();
                while(it.hasNext()) {
                    SubNetInfo subNetInfo = (SubNetInfo)it.next();
                    String reNetInfo = subNetInfo.getNetInfo();
                    if (!reNetInfo.equalsIgnoreCase(netInfo)) {
                        ConcurrentLinkedQueue  searchDeviceList = (new SearchNodeService()).searchDevice(reNetInfo, snmpTargets, isUsePing);
                        deviceInfos.addAll(searchDeviceList);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
