package com.seven.smartsnmp.discover.service;

import com.seven.smartsnmp.base.MyException;
import com.seven.smartsnmp.config.DiscoverConfig;
import com.seven.smartsnmp.discover.DeviceInfo;
import com.seven.smartsnmp.discover.ResourceInfo;
import com.seven.smartsnmp.discover.ResourceSearchMothType;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class SearchResourceService {
    private List<ResourceInfo> rsList = new CopyOnWriteArrayList();

    public SearchResourceService() {
    }

    public List<ResourceInfo> searchDeviceResource(DeviceInfo var1) throws MyException {
        List var2 = DiscoverConfig.getAllResourceType();
        int var3 = var2.size();
        ThreadPoolExecutor var4 = new ThreadPoolExecutor(var3, var3, 30L, TimeUnit.SECONDS, new LinkedBlockingQueue());
        Iterator var6 = var2.iterator();

        while(var6.hasNext()) {
            ResourceSearchMothType var5 = (ResourceSearchMothType)var6.next();
            var4.execute(new ResourceSearchTask(var1, var5, this.rsList));
        }

        var4.shutdown();

        try {
            for(boolean var8 = false; !var8; var8 = var4.awaitTermination(1L, TimeUnit.SECONDS)) {
            }
        } catch (InterruptedException var7) {
            var7.printStackTrace();
        }

        return this.rsList;
    }
}
