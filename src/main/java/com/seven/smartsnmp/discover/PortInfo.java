package com.seven.smartsnmp.discover;


import com.seven.smartsnmp.mib.Dot1dBasePortEntry;
import com.seven.smartsnmp.mib.MibIfEntry;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author seven
 * @create 2019-06-17 10:16
 **/
public class PortInfo implements Serializable {
    public DeviceInfo device;
    public int port = -1;
    public boolean isUseIfIndexPort = false;
    public List<String> portMacList = new CopyOnWriteArrayList();
    public List<DeviceInfo> subDeviceList = new CopyOnWriteArrayList();

    public PortInfo() {
    }

    @Override
    public String toString() {
        return this.device.toString() + this.port;
    }

    public MibIfEntry getPortMibIfEntry() {
        int var1 = -2147483648;
        Iterator var3;
        if (this.isUseIfIndexPort) {
            var1 = this.port;
        } else {
            var3 = this.device.getBasePortList().iterator();

            while (var3.hasNext()) {
                Dot1dBasePortEntry var2 = (Dot1dBasePortEntry) var3.next();
                if (var2.getDot1dBasePort() == this.port) {
                    var1 = var2.getDot1dBasePortIfIndex();
                }
            }
        }

        var3 = this.device.getIfTableList().iterator();

        while (var3.hasNext()) {
            MibIfEntry var4 = (MibIfEntry) var3.next();
            if (var4.getIfIndex() == var1) {
                return var4;
            }
        }

        return null;
    }

    @Override
    public int hashCode() {
        byte var1 = 1;
        int var2 = 31 * var1 + (this.device == null ? 0 : this.device.hashCode());
        var2 = 31 * var2 + this.port;
        return var2;
    }

    @Override
    public boolean equals(Object var1) {
        if (this == var1) {
            return true;
        } else if (var1 == null) {
            return false;
        } else if (!(var1 instanceof PortInfo)) {
            return false;
        } else {
            PortInfo var2 = (PortInfo) var1;
            if (this.device == null) {
                if (var2.device != null) {
                    return false;
                }
            } else if (!this.device.equals(var2.device)) {
                return false;
            }

            return this.port == var2.port;
        }
    }
}
