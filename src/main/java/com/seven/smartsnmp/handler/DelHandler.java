package com.seven.smartsnmp.handler;

import com.seven.smartsnmp.base.MyException;
import org.snmp4j.Snmp;
import org.snmp4j.Target;

/**
 * @author Admin
 */
public class DelHandler {

    public  void delTable(Object obj, Snmp snmp, Target target) throws MyException {
        HandlerFactory.getUpdateHandler().update(obj, snmp, target);
    }
}
