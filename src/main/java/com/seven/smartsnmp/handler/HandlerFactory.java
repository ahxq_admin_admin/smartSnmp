package com.seven.smartsnmp.handler;

/**
 * 创建单例模式的处理器
 * @author Admin
 */
public class HandlerFactory {

    private volatile static  AddHandler addHandler;
    private volatile static  DelHandler delHandler;
    private volatile static  RefreshHandler refreshHandler;
    private volatile static  UpdateHandler updateHandler;
    private volatile static  SetHandler setHandler;
    private volatile static  SelectHandler selectHandler;
    public static AddHandler getAddHandler() {
        if (addHandler == null) {
            synchronized (HandlerFactory.class) {
                if (addHandler == null) {
                    addHandler = new AddHandler();
                }
            }
        }
        return addHandler;
    }
    public static DelHandler getDelHandler() {
        if (delHandler == null) {
            synchronized (HandlerFactory.class) {
                if (delHandler == null) {
                    delHandler = new DelHandler();
                }
            }
        }
        return delHandler;
    }
    public static RefreshHandler getRefreshHandler() {
        if (refreshHandler == null) {
            synchronized (HandlerFactory.class) {
                if (refreshHandler == null) {
                    refreshHandler = new RefreshHandler();
                }
            }
        }
        return refreshHandler;
    }
    public static UpdateHandler getUpdateHandler() {
        if (updateHandler == null) {
            synchronized (HandlerFactory.class) {
                if (updateHandler == null) {
                    updateHandler = new UpdateHandler();
                }
            }
        }
        return updateHandler;
    }

    public static SetHandler getSetHandler() {
        if (setHandler == null) {
            synchronized (HandlerFactory.class) {
                if (setHandler == null) {
                    setHandler = new SetHandler();
                }
            }
        }
        return setHandler;
    }
    public static SelectHandler getSelectHandler() {
        if (selectHandler == null) {
            synchronized (HandlerFactory.class) {
                if (selectHandler == null) {
                    selectHandler = new SelectHandler();
                }
            }
        }
        return selectHandler;
    }


}
