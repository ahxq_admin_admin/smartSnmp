package com.seven.smartsnmp.trap;

import com.seven.smartsnmp.base.MyException;
import com.seven.smartsnmp.base.MyLog;
import org.snmp4j.CommandResponder;
import org.snmp4j.CommandResponderEvent;
import org.snmp4j.PDU;
import org.snmp4j.PDUv1;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;

import java.util.Iterator;
import java.util.Vector;

public class TrapReceiverImpl implements CommandResponder {



    @Override
    public void processPdu(CommandResponderEvent var1) {
        try {
            System.out.println("Reciever SNMP TRAP PDU：" + var1.getPDU());
            UdpAddress var2 = (UdpAddress) var1.getPeerAddress();
            String var3 = var2.getInetAddress().getHostAddress();
            PDU var4 = var1.getPDU();
            System.out.println("The TRAP Message:" + var4);
            TrapMessageInfo var5 = new TrapMessageInfo();
            var5.setAgentSendIP(var3);
            this.setOIDList(var5, var4);
            this.setValueList(var5, var4);
            var5.setOriginalPDU(var4);
            if (var4 instanceof PDUv1) {
                PDUv1 var6 = (PDUv1) var4;
                var5.setTrapVersion(1);
                String var7 = var6.getEnterprise().toString();
                var5.setTrapOID(var7);
                String var8 = var6.getAgentAddress().toString();
                var5.setPduAgentIP(var8);
                var5.setTrapV1GenericType(var6.getGenericTrap());
                if (var6.getGenericTrap() == 6) {
                    var5.setTrapV1SpecificType(var6.getSpecificTrap());
                }

                var5.setTrapName(var5.getTrapOID());
                switch (var6.getGenericTrap()) {
                    case 0:
                        var5.setTrapName("设备冷启动");
                        break;
                    case 1:
                        var5.setTrapName("设备热启动");
                        break;
                    case 2:
                        var5.setTrapName("接口关闭");
                        break;
                    case 3:
                        var5.setTrapName("接口启用");
                        break;
                    case 4:
                        var5.setTrapName("SNMP认证失败");
                        break;
                    case 5:
                        var5.setTrapName("EGP邻居丢失");
                }
            } else {
                var5.setTrapVersion(2);
                Variable var10 = var5.getOIDValue(SnmpConstants.snmpTrapAddress.toString());
                if (var10 != null) {
                    var5.setPduAgentIP(var10.toString());
                }

                Variable var11 = var5.getOIDValue(SnmpConstants.snmpTrapOID.toString());
                if (var11 != null) {
                    var5.setTrapOID(var11.toString());
                }

                var5.setTrapName(var5.getTrapOID());
                if (var11 != null) {
                    if (var11.equals(SnmpConstants.coldStart)) {
                        var5.setTrapName("设备冷启动");
                    } else if (var11.equals(SnmpConstants.warmStart)) {
                        var5.setTrapName("设备热启动");
                    } else if (var11.equals(SnmpConstants.linkDown)) {
                        var5.setTrapName("接口关闭");
                    } else if (var11.equals(SnmpConstants.linkUp)) {
                        var5.setTrapName("接口启动");
                    } else if (var11.equals(SnmpConstants.authenticationFailure)) {
                        var5.setTrapName("SNMP认证失败");
                    }
                }
            }

            this.sendTrap(var5);
        } catch (Exception var9) {
            MyLog.err("Process TRAP PDU Erro:" + var9.getMessage());
        }

    }

    void sendTrap(TrapMessageInfo var1) throws MyException {
        synchronized (TrapReceiverServer.eventQueue) {
            TrapReceiverServer.eventQueue.addLast(var1);
            TrapReceiverServer.eventQueue.notifyAll();
        }
    }

    void setOIDList(TrapMessageInfo var1, PDU var2) {
        Vector var3 = var2.getVariableBindings();
        Iterator var5 = var3.iterator();

        while (var5.hasNext()) {
            VariableBinding var4 = (VariableBinding) var5.next();
            String var6 = var4.getOid().toString();
            var1.getTrapPDUOIDs().add(var6);
        }

    }

    void setValueList(TrapMessageInfo var1, PDU var2) {
        Vector var3 = var2.getVariableBindings();
        Iterator var5 = var3.iterator();

        while (var5.hasNext()) {
            VariableBinding var4 = (VariableBinding) var5.next();
            Variable var6 = var4.getVariable();
            var1.getTrapPDUOIDValues().add(var6);
        }

    }
}
