package com.seven.smartsnmp.trap;

import org.snmp4j.PDU;
import org.snmp4j.smi.Variable;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class TrapMessageInfo {
    private String agentSendIP = "";
    private String pduAgentIP = "";
    private Timestamp receiverTime = new Timestamp(System.currentTimeMillis());
    private int trapVersion = 1;
    private int trapV1GenericType = -1;
    private int trapV1SpecificType = -1;
    private String trapName = "";
    private String trapOID = "";
    private List<String> trapPDUOIDs = new ArrayList();
    private List<Variable> trapPDUOIDValues = new ArrayList();
    private PDU originalPDU;
    public static final int TrapVersionV1 = 1;
    public static final int TrapVersionV2 = 2;

    public Variable getOIDValue(String trapOID) {
        for(int i = 0; i < this.trapPDUOIDs.size(); ++i) {
            String oid = (String)this.trapPDUOIDs.get(i);
            if (trapOID.equals(oid)) {
                return (Variable)this.trapPDUOIDValues.get(i);
            }
        }
        return null;
    }

    public String getAgentSendIP() {
        return agentSendIP;
    }

    public void setAgentSendIP(String agentSendIP) {
        this.agentSendIP = agentSendIP;
    }

    public String getPduAgentIP() {
        return pduAgentIP;
    }

    public void setPduAgentIP(String pduAgentIP) {
        this.pduAgentIP = pduAgentIP;
    }

    public Timestamp getReceiverTime() {
        return receiverTime;
    }

    public void setReceiverTime(Timestamp receiverTime) {
        this.receiverTime = receiverTime;
    }

    public int getTrapVersion() {
        return trapVersion;
    }

    public void setTrapVersion(int trapVersion) {
        this.trapVersion = trapVersion;
    }

    public int getTrapV1GenericType() {
        return trapV1GenericType;
    }

    public void setTrapV1GenericType(int trapV1GenericType) {
        this.trapV1GenericType = trapV1GenericType;
    }

    public int getTrapV1SpecificType() {
        return trapV1SpecificType;
    }

    public void setTrapV1SpecificType(int trapV1SpecificType) {
        this.trapV1SpecificType = trapV1SpecificType;
    }

    public String getTrapName() {
        return trapName;
    }

    public void setTrapName(String trapName) {
        this.trapName = trapName;
    }

    public String getTrapOID() {
        return trapOID;
    }

    public void setTrapOID(String trapOID) {
        this.trapOID = trapOID;
    }

    public List<String> getTrapPDUOIDs() {
        return trapPDUOIDs;
    }

    public void setTrapPDUOIDs(List<String> trapPDUOIDs) {
        this.trapPDUOIDs = trapPDUOIDs;
    }

    public List<Variable> getTrapPDUOIDValues() {
        return trapPDUOIDValues;
    }

    public void setTrapPDUOIDValues(List<Variable> trapPDUOIDValues) {
        this.trapPDUOIDValues = trapPDUOIDValues;
    }

    public PDU getOriginalPDU() {
        return originalPDU;
    }

    public void setOriginalPDU(PDU originalPDU) {
        this.originalPDU = originalPDU;
    }
}
